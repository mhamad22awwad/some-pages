<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Multi Step Form</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


    <style>
        body{
            background-color: #F6F7F8;
        }
        .cardInfo {
            width: 330px;
            display: block;
            float: left;
        }
        .form {
            width: 691px;
            display: block;
            float: left;
        }
        .orderTextSize {
            font-size: 20px;
        }
        .active_tab1 {
            background-color: #fff;
            color: #333;
            font-weight: 600;
        }

        .inactive_tap1 {
            background-color: #f5f5f5;
            color: #333;
        }

        .has-error {
            border-color: #cc0000;
            background-color: #ffff99;
        }


    </style>

</head>
<body>

    <div class="h-auto d-inline-block w-100 px-3">
        <div class="container">
            <div class="text-left">	
                <div class=" cardInfo form ml-4 m-3 ">
                    <div class="card ">
                        <div class="card-body ">

                            <h3 class="card-title order orderTextSize"> ACCOUNT </h3>
                            <p class="card-text mt-3 pt-3"> To place your order now, log in to your existing account or sign up.</p>
                                        
                            <form id="register_form" method="post" accept-charset="utf-8">
                                    
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <button  type="button" id="list_login_details" name="list_login_details" class="nav-link active_tab1" >Login Details</button>
                                    </li>
                                    <li class="nav-item">
                                        <button disabled type="button" id="list_personal_details" name="list_personal_details" class="nav-link inactive_tap1" >Personal Details</button>
                                    </li>
                                    <li class="nav-item">
                                        <button disabled type="button" id="list_birth_day_details" name="list_birth_day_details" class="nav-link inactive_tap1" >Choose your birthday</button>
                                    </li>
                                    <li class="nav-item">	
                                        <button disabled type="button" id="list_contact_details" name="list_contact_details" class="nav-link inactive_tap1" >Contact Details</button>
                                    </li>
                                </ul>

                                <div class="tab-content mt-3">
                                    <div id="login_details" class="tab-pane active">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Login Details </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="email">Enter Email Address</label>
                                                    <input id="email" class="form-control the_re_val" type="text" name="email" placeholder="Enter Email Address">
                                                    <span id="error_email" class="text-danger"></span>	
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">Enter Password</label>
                                                    <input id="password" class="form-control the_re_val" type="password" name="password" placeholder="">
                                                    <span id="error_password" class="text-danger"></span>
                                                </div>
                                                <div class="text-center">
                                                    <button id="btn_login_details" name="btn_login_details" class="btn btn-info" type="button">Next</button>
                                                </div>
                                            </div>
                                        </div>	
                                    </div>

                                    <div id="personal_details" class="tab-pane fade">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Fill Personal Details </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="first_name">Enter First Name</label>
                                                    <input id="first_name" type="text" name="first_name" class="form-control the_re_val" placeholder="">
                                                    <span id="error_first_name" class="text-danger"></span>
                                                </div>
                                                <div class="form-group">
                                                    <label for="last_name">Enter Last Name</label>
                                                    <input id="last_name" type="text" name="last_name" class="form-control the_re_val" placeholder="">
                                                    <span id="error_last_name" class="text-danger"></span>
                                                </div>
                                                <div class="text-center">
                                                    <button id="previous_btn_personal_details" class="btn btn-default " type="button" name="previous_btn_personal_details">Previous</button>
                                                    <button id="btn_personal_details" class="btn btn-info" type="button" name="btn_personal_details" >Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="birth_day_details" class="tab-pane fade">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Selaect Birthe Day </div>
                                            <div class="panel-body">			
                                                <div class="form-group">
                                                    <label for="birth_day">selest date</label>
                                                    <input id="birth_day" type="date" class="the_re_val" name="birth_day" >
                                                    <span id="error_birth_day"></span>
                                                </div>
                                                <div id="the_gender" class="form-group" >
                                                    <label>Gender - </label>
                                                    <label for="male">Male</label>
                                                    <input id="male" class="gender the_radio" type="radio" name="gender" value="male" >
                                                    <label for="female">Female</label>
                                                    <input id="female" class="gender the_radio" type="radio" name="gender" value="female" >			
                                                    <span id="error_gender"></span>
                                                </div>
                                                <br>
                                                <div class="text-center">
                                                    <button id="previous_btn_birth_day" class="btn btn-default" type="button" name="previous_btn_birth_day">Previous</button>
                                                    <button id="btn_birth_day" class="btn btn-info" type="button" name="btn-birth_day">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="contact_details" class="tab-pane fade">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Fill Contact Details </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="address">Enter Address</label>
                                                    <textarea id="address" class="form-control the_re_val" name="address"></textarea>
                                                    <span id="error_address" class="text-danger"></span>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Enter Mobile Number</label>
                                                    <input id="mobile_no" class="form-control the_re_val" type="text" name="mobile_no" placeholder="">
                                                    <span id="error_mobile_no" class="text-danger"></span>
                                                </div>
                                                <br>
                                                <div class="text-center">
                                                    <button id="previous_btn_contact_details" class="btn btn-default " type="button" name="previous_btn_contact_details"> Previous </button>
                                                    <button id="btn_contact_details" class="btn btn-success" type="button" name="btn_contact_details">Register</button>
                                                </div>	
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        
                        </div>
                    </div>
                </div>
            </div>		
        </div>
    </div>

</body>

</html>


<script>

	$(document).ready(function(){

		//go from the first tap the email & password to the next tap the personal details
		$('#btn_login_details').click(function(){
			var error_email = '';
			var error_password = '';
			var filter = /\S+@\S+\.\S+/;
            // see if there any data in the email input and see if it true 
			if ($.trim($('#email').val()).length == 0) {
				error_email = 'Email is required';
				$('#error_email').text(error_email);
				$('#email').addClass('has-error');
			} else {
				if (!filter.test($('#email').val())) {
					error_email = 'Invalid Email';
					$('#error_email').text(error_email);
					$('#email').addClass('has-error');
				} else {
					error_email = '';
					$('#error_email').text(error_email);
					$('#email').removeClass('has-error');
				}
			}
            // see if there any password in the password input
			if ($.trim($('#password').val()).length == 0) {
				error_password = 'Password is required';
				$('#error_password').text(error_password);
				$('#password').addClass('has-error');
			} else {
				error_password = '';
				$('#error_password').text(error_password);
				$('#password').removeClass('has-error');
			}
            // see if there any error in the email & password input
            // and then move to the next tap and lit it active
			if (error_email != '' || error_password != '') {
				return false;
			} else {

				$('#list_login_details').removeClass('active_tab1');
				$('#list_login_details').addClass('inactive_tap1');
				
				$('#list_personal_details').removeClass('inactive_tap1');
				$('#list_personal_details').addClass('active_tab1');

				//enabel the taps
				$('#list_personal_details').removeAttr('disabled');

				

				$('#login_details').removeClass('active');
				$('#login_details').addClass('fade');


				$('#personal_details').addClass('active');
				$('#personal_details').removeClass('fade');

			}
		});


		//back from the personal detail to login details
		$('#previous_btn_personal_details').click(function(){

			$('#list_personal_details').removeClass('active active_tab1');
			$('#list_personal_details').addClass('inactive_tap1');

			$('#list_login_details').addClass('active_tab1');
			$('#list_login_details').removeClass('inactive_tap1');

			$('#personal_details').removeClass('active');
			$('#personal_details').addClass('fade');

			$('#login_details').removeClass('fade');
			$('#login_details').addClass('active');


		});


		//go from the second tap personal details to the next tap the birthday
		$('#btn_personal_details').click(function(){

			var error_first_name = '';
			var error_last_name = '';
            
            // see if there is any name enterd
			if ($.trim($('#first_name').val()).length == 0) {
				
				error_first_name = 'Enter First Name';
				$('#error_first_name').text(error_first_name);
				$('#first_name').addClass('has-error');

			} else {
				
				error_first_name = '';
				$('#error_first_name').text(error_first_name);
				$('#first_name').removeClass('has-error');

			}
            // see the second name 
			if ($.trim($('#last_name').val()).length == 0) {

				error_last_name = 'Enter last name';
				$('#error_last_name').text(error_last_name);
				$('#last_name').addClass('has-error');
				
			} else {

				error_last_name = '';
				$('#error_last_name').text(error_last_name);
				$('#last_name').removeClass('has-error');
				
			}
            // see if the first & the last name is true it will go to the 3ed tap
			if (error_first_name != '' || error_last_name != '' ) {
				return false;
			} else {

				$('#list_personal_details').removeClass(' active_tab1');
				$('#list_personal_details').addClass('inactive_tap1');

				$('#list_birth_day_details').removeClass('inactive_tap1');
				$('#list_birth_day_details').addClass('active_tab1');
				$('#list_birth_day_details').removeAttr('disabled');

				$('#personal_details').removeClass('active');
				$('#personal_details').addClass('fade');

				$('#birth_day_details').removeClass('fade');
				$('#birth_day_details').addClass('active');

				//in_tap = 2;

				
			}
		});


		//back from the birthday to personal details
		$('#previous_btn_birth_day').click(function(){

			$('#list_birth_day_details').removeClass('active_tab1');
			$('#list_birth_day_details').addClass('inactive_tap1');

			$('#list_personal_details').removeClass('inactive_tap1');
			$('#list_personal_details').addClass('active_tab1');

			$('#birth_day_details').removeClass('active');
			$('#birth_day_details').addClass('fade');

			$('#personal_details').removeClass('fade');
			$('#personal_details').addClass('active');

		});


		//go from the second tap to the next tap the contact details 
		$('#btn_birth_day').click(function(){
			var error_birth_day = '';
			var error_gender = '';

			var radioValue = $("input[name='gender']:checked").val();			
			
            // see the selected birth day by the user
			if ($.trim($('#birth_day').val()).length == 0) {

				error_birth_day = 'Birth Day is required';
				$('#error_birth_day').text(error_birth_day);
				$('#birth_day').addClass('has-error');

			} else {

				error_birth_day = '';
				$('#error_birth_day').text(error_birth_day);
				$('#birth_day').removeClass('has-error');
				
			}

            // see if the user sellect a gender
			if (radioValue == null ) {
				
				error_gender = 'Gender is required';
				$('#error_gender').text(error_gender);
				$('#the_gender').addClass('has-error');

			} else {

				error_gender = '';
				$('#error_gender').text(error_gender);
				$('#the_gender').removeClass('has-error');
				
			}

            // see if the birth day and the gender is true to go to the next tap
			if (error_birth_day != '' || error_gender != '' ) {
				
				return false;

			} else {
				
				$('#list_birth_day_details').removeClass('active_tab1');
				$('#list_birth_day_details').addClass('inactive_tap1');

				$('#list_contact_details').removeClass('inactive_tap1');
				$('#list_contact_details').addClass('active_tab1');
				$('#list_contact_details').removeAttr('disabled');

				$('#birth_day_details').removeClass('active');
				$('#birth_day_details').addClass('fade');

				$('#contact_details').removeClass('fade');
				$('#contact_details').addClass('active');

			}
		});

		//back from the contact details to birth day details
		$('#previous_btn_contact_details').click(function(){

			$('#list_contact_details').removeClass('active_tab1');
			$('#list_contact_details').addClass('inactive_tap1');

			$('#list_birth_day_details').removeClass('inactive_tap1');
			$('#list_birth_day_details').addClass('active_tab1');

			$('#contact_details').removeClass('active');
			$('#contact_details').addClass('fade');

			$('#birth_day_details').removeClass('fade');
			$('#birth_day_details').addClass('active');

		});

		//finsh the steps & submit the data of the user
		$('#btn_contact_details').click(function(){

			var error_address = '';
			var error_mobile_no = '';

            // see all the enterd data and get them data
			var the_email = $('#email').val();
			var the_password = $('#password').val();
			var the_first_name = $('#first_name').val();
			var the_last_name = $('#last_name').val();
			var the_birth_day = $('#birth_day').val();
			var the_gender = $("input[name='gender']:checked").val();
			var the_address = $('#address').val();
			var the_mobile_no = $('#mobile_no').val();

            // see the address info if it enterd
			if ($.trim($('#address').val()).length == 0) {
				
				error_address = 'Address is required';
				$('#error_address').text(error_address);
				$('#address').addClass('has-error');

			} else {

				error_address = '';
				$('#error_address').text(error_address);
				$('#address').removeClass('has-error');
				
			}
            // see the phone number if it enterd
			if ($.trim($('#mobile_no').val()).length == 0) {

				error_mobile_no = 'Phone Numbre is required';
				$('#error_mobile_no').text(error_mobile_no);
				$('#mobile_no').addClass('has-error');
				
			} else {

				error_mobile_no = '';
				$('#error_mobile_no').text(error_mobile_no);
				$('#mobile_no').removeClass('has-error');
				
			}

            // if all the data true it will submit the data by use Ajax
			if (error_mobile_no != '' || error_address != '') {
				return false;
			} else {
				
				$.ajax({
					type: 'POST',
					url: 'add_the_new_user.php',
					data: { 
						email : the_email ,
						password : the_password ,
						first_name : the_first_name ,
						last_name : the_last_name ,
						birth_day : the_birth_day ,
						gender : the_gender ,
						address : the_address ,
						mobile_no : the_mobile_no 
					},

					success: function(data){
                        // clear the form from all the data if hte data is inserted 

                        alert('you account have been created ');

                        $('.the_re_val').val("");

                        $('.the_radio').prop('checked', false);
                        
                        $('#list_contact_details').removeClass('active_tab1');
                        $('#list_contact_details').addClass('inactive_tap1');

                        $('#list_login_details').removeClass('inactive_tap1');
                        $('#list_login_details').addClass('active_tab1');

                        $('#contact_details').removeClass('active');
                        $('#contact_details').addClass('fade');

                        $('#login_details').removeClass('fade');
                        $('#login_details').addClass('active');

                        $('#list_personal_details').prop('disabled', true);
                        $('#list_birth_day_details').prop('disabled', true);
                        $('#list_contact_details').prop('disabled', true);

                    }
				});
			}
		}
        );

		//go to the 1 tap and hide all taps
		$('#list_login_details').click(function(){
				
			$('#list_personal_details').removeClass('active_tab1');
			$('#list_personal_details').addClass('inactive_tap1');
			
			$('#list_login_details').removeClass('inactive_tap1');
			$('#list_login_details').addClass('active_tab1');
			
			$('#list_personal_details').removeClass('active_tab1');
			$('#list_personal_details').addClass('inactive_tap1');

			$('#list_birth_day_details').removeClass('active_tab1');
			$('#list_birth_day_details').addClass('inactive_tap1');
			
			$('#list_contact_details').removeClass('active_tab1');
			$('#list_contact_details').addClass('inactive_tap1');

			
			$('#login_details').removeClass('fade');
			$('#login_details').addClass('active');

			$('#personal_details').removeClass('active');
			$('#personal_details').addClass('fade');

			$('#birth_day_details').removeClass('active');
			$('#birth_day_details').addClass('fade');

			$('#contact_details').removeClass('active');
			$('#contact_details').addClass('fade');


		});

		//go to the 2 tap and hide all taps
		$('#list_personal_details').click(function(){

			$('#list_personal_details').removeClass('inactive_tap1');
			$('#list_personal_details').addClass('active_tab1');

			$('#list_login_details').removeClass('active_tab1');
			$('#list_login_details').addClass('inactive_tap1');

			$('#list_birth_day_details').removeClass('active_tab1');
			$('#list_birth_day_details').addClass('inactive_tap1');

			$('#list_contact_details').removeClass('active_tab1');
			$('#list_contact_details').addClass('inactive_tap1');


			$('#personal_details').removeClass('fade');
			$('#personal_details').addClass('active');

			$('#login_details').removeClass('active');
			$('#login_details').addClass('fade');

			$('#birth_day_details').removeClass('active');
			$('#birth_day_details').addClass('fade');

			$('#contact_details').removeClass('active');
			$('#contact_details').addClass('fade');

		});

		//go to the 3 tap and hide all taps
		$('#list_birth_day_details').click(function(){

			$('#list_birth_day_details').removeClass('inactive_tap1');
			$('#list_birth_day_details').addClass('active_tab1');

			$('#list_login_details').removeClass('active_tab1');
			$('#list_login_details').addClass('inactive_tap1');

			$('#list_personal_details').removeClass('active_tab1');
			$('#list_personal_details').addClass('inactive_tap1');

			$('#list_contact_details').removeClass('active_tab1');
			$('#list_contact_details').addClass('inactive_tap1');


			$('#birth_day_details').removeClass('fade');
			$('#birth_day_details').addClass('active');

			$('#login_details').removeClass('active');
			$('#login_details').addClass('fade');

			$('#personal_details').removeClass('active');
			$('#personal_details').addClass('fade');

			$('#contact_details').removeClass('active');
			$('#contact_details').addClass('fade');


		});
		
		//go to the 4 tap and hide all taps
		$('#list_contact_details').click(function(){

			$('#list_contact_details').removeClass('inactive_tap1');
			$('#list_contact_details').addClass('active_tab1');

			$('#list_login_details').removeClass('active_tab1');
			$('#list_login_details').addClass('inactive_tap1');

			$('#list_personal_details').removeClass('active_tab1');
			$('#list_personal_details').addClass('inactive_tap1');

			$('#list_birth_day_details').removeClass('active_tab1');
			$('#list_birth_day_details').addClass('inactive_tap1');


			$('#contact_details').removeClass('fade');
			$('#contact_details').addClass('active');

			$('#login_details').removeClass('active');
			$('#login_details').addClass('fade');

			$('#personal_details').removeClass('active');
			$('#personal_details').addClass('fade');

			$('#birth_day_details').removeClass('active');
			$('#birth_day_details').addClass('fade');

		});
    });

</script>