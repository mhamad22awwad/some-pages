<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">

	<?php include('include/links.php') ?>


</head>
<body id="page6">
<?php include('include/nav_bar.php') ?>



<div id="part1" class="h-auto d-inline-block w-100 p-3 pt-5">
	<div class="container">
		<div class="text-left">
			
				<div class=" cardInfo ml-4 m-3">
					<div class="card ">
						
						<div class="card-body ">
							<div class="imgHold mr-2">
	  							<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  						</div>
							<h3 class="card-title order orderTextSize"> ACCOUNT </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing. </p>
			            </div>
			        	
			        </div>
				</div>


				<div class=" cardInfo m-3">
					<div class="card ">
						
						<div class="card-body ">
							<div class="imgHold mr-2">
	  							<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  						</div>
							<h3 class="card-title order orderTextSize"> DELIVERY ADDRESS </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing. </p>
			            </div>
			        	
			        </div>
				</div>


				<div class=" cardInfo mr-4 m-3">
					<div class="card ">

						<div class="card-body ">
							<div class="imgHold mr-2">
	  							<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  						</div>
							<h3 class="card-title order orderTextSize"> PAYMENT </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing. </p>
			            </div>
			        	
			        </div>
				</div>

			
		</div>		
	</div>
</div>

<div id="part2" class="h-auto d-inline-block w-100 px-3">
	<div class="container">
		<div class="text-left">
			
				<div class=" cardInfo form ml-4 m-3 ">
					<div class="card ">
						<div class="card-body ">

							<h3 class="card-title order orderTextSize"> SELECT DELIVERY ADDRESS </h3>
			            	<p class="card-text "> You have a saved address in this location</p>
			            	
			            	<div class="my-3">



			            		<div class=" locetionInfo ml-0 mr-3 my-1">
									<div class="card locetionBorder">

										<div class="card-body my-2">
											<div class="locetionHome mr-2 ">
	  											<i class="fas fa-home"></i>
	  										</div>
	  										<div class="locetion ">
												<address class="mb-0 pb-1">
													Kelley A. Fleming 196 Woodside <br>
													Circle Mobile, FL 36602 <br>
													Phone:240-256-1942 <br>
												</address>
												<p>26 Mint</p>
												<a href="#" class="p-2 butt buttuns text-white mx-auto ">DELIVERY HERE</a>

			            					</div>
			            				</div>
			        	
			        				</div>
								</div>
								

								<div class=" locetionInfo mx-2 my-1">
									<div class="card ">

										<div class="card-body my-2">
											<div class="locetionHome mr-2 ">
	  											<i class="fas fa-home"></i>
	  										</div>
	  										<div class="locetion ">
												<address class="mb-0 pb-1">
													Carol J. Stephens 1635 Franklin <br>
													Street Montgomery, AL 36104 <br>
													Phone:126-632-2345 <br>
												</address>
												<p>26 Mint</p>
												<a href="#" class="p-2 butt border-dark text-dark  mx-auto ">DELIVERY HERE</a>

			            					</div>
			            				</div>
			        	
			        				</div>
								</div>



			            	</div>
			            	

			            </div>
			        </div>
				</div>


				


				<div id="" class="card cardInfo m-3  text-left bg-white px-2" style="">
  	
  							<div class="card-header bg-white borderCard">
  								<div class="imgCardHold mr-4">
  									<img src="https://via.placeholder.com/100" alt="">
  								</div>
  								<div class="text-left py-1">
  									<h4> GOOD MEAL </h4>
  									<p> Newrownads </p>
  									<p class=""> VIEW FULL MENU </p>
  								</div>
							</div>

							<div class=" text-dark px-2">
		    					<h5 class=" order  mt-2 mb-2 pt-2 ">Sandwich
									<div class="cartFont">
										<span class="">$35</span>			
									</div>
								</h5>

								<h5 class=" order  mt-2 mb-0 pt-3 ">Sub total
									<div class="cartFont">
										<span class="">$35</span>
									</div>
								</h5>

								<p> Extra charges may apply </p>

								<a href="#" class="butt p-2 text-center ordNow w-100 text-white mx-auto mb-2">CHECK OUT NOW</a>
							</div>
						</div>

			
		</div>		
	</div>
</div>







<?php include('include/footre.php') ?>	
</body>
</html>