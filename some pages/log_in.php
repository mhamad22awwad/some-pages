<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">

	<?php include('include/links.php') ?>


</head>
<body id="page4">
<?php include('include/nav_bar.php') ?>



<div id="part1" class="h-auto d-inline-block w-100 p-3 pt-5">
	<div class="container">
		<div class="text-left">
			
				<div class=" cardInfo ml-4 m-3">
					<div class="card ">
						
						<div class="card-body ">
							<div class="imgHold mr-2">
	  							<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  						</div>
							<h3 class="card-title order orderTextSize"> ACCOUNT </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing. </p>
			            </div>
			        	
			        </div>
				</div>


				<div class=" cardInfo m-3">
					<div class="card ">
						
						<div class="card-body ">
							<div class="imgHold mr-2">
	  							<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  						</div>
							<h3 class="card-title order orderTextSize"> DELIVERY ADDRESS </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing. </p>
			            </div>
			        	
			        </div>
				</div>


				<div class=" cardInfo mr-4 m-3">
					<div class="card ">

						<div class="card-body ">
							<div class="imgHold mr-2">
	  							<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  						</div>
							<h3 class="card-title order orderTextSize"> PAYMENT </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing. </p>
			            </div>
			        	
			        </div>
				</div>

			
		</div>		
	</div>
</div>

<div id="part2" class="h-auto d-inline-block w-100 px-3">
	<div class="container">
		<div class="text-left">
			
				<div class=" cardInfo form ml-4 m-3 ">
					<div class="card ">
						<div class="card-body ">

							<h3 class="card-title order orderTextSize"> ACCOUNT </h3>
			            	<p class="card-text mt-3 pt-3"> To place your order now, log in to your existing account or sign up.</p>
			            	<div class="mt-3 pt-3">
			            		<a href="#" title=""> Sign up</a><span> or </span><a href="#" class="text-danger" title="">log in to account</a>
			            	</div>
			            	
			            	<form class="mt-3 pt-3" action="#" >

					    		<div class="input-group   mt-1">
						    		<input type="text" class="form-control mr-4" placeholder="Enter mobile number">
						    		<input type="text" class="form-control " placeholder="Password">
						    		<br>
						     		<button type="submit" class="butt p-2 searchBut w-100 px-4 mt-4 buttuns text-white" > SIGN IN </button>
					  			</div>
			  			
			  				</form>

			            </div>
			        </div>
				</div>


				


				<div id="" class="card cardInfo m-3  text-left bg-white px-2" style="">
  	
  							<div class="card-header bg-white borderCard">
  								<div class="imgCardHold mr-4">
  									<img src="https://via.placeholder.com/100" alt="">
  								</div>
  								<div class="text-left ">
  									<h4> GOOD MEAL </h4>
  									<p> Newrownads </p>
  									<p class=""> VIEW FULL MENU </p>
  								</div>
							</div>

							<div class=" text-dark px-2 ">
		    					<h5 class=" order  mt-2 mb-2 pt-2 ">Sandwich
									<div class="cartFont">
										<span class="">$35</span>			
									</div>
								</h5>

								<h5 class=" order  mt-2 mb-0 pt-3 ">Sub total
									<div class="cartFont">
										<span class="">$35</span>
									</div>
								</h5>

								<p> Extra charges may apply </p>

								<a href="#" class="butt p-2 ordNow w-100 text-white text-center mx-auto mb-2">CHECK OUT NOW</a>
							</div>
						</div>

			
		</div>		
	</div>
</div>







<?php include('include/footre.php') ?>	
</body>
</html>