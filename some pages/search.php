<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Canvas</title>
	

	<?php include('include/links.php') ?>


</head>

<body id="page2">
<?php include('include/nav_bar.php') ?>
	



<div id="page2Part1" class="text-white bg-dark h-auto d-inline-block w-100 mb-5">

	<div id="slaider" class="container height">

		<div class="text-center mx-auto " >

			<h4 class="mb-4">FOOD DELIVERY IN NEWTOWNARDS</h4>

			
			
					<form class="form-inline" action="#" >
			    		<div class="input-group  mx-auto mt-1">
			    		<input type="text" class="form-control search" placeholder="Enter your delivery addressh">
			   			<div class="input-group-append">
			     		<button type="submit" class="btn searchBut px-4" > SEARCH </button>
			    		</div>
			  			</div>

			  			<div id="livesearch">
			  				
			  			</div>
			  		</form>
		  		
  		</div>
  	</div>

</div>

	

<div id="page2Part2" class="container my-5">
	<div class="row m-auto">
		<div class="col-12 pl-4">
			<h1 style=""> POPULAR BRANDS </h1>
			<button type="button" class="btn btn-outline-dark mr-3 " style=""> FILTER </button>
		</div>


			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordreNew nameCatOrd">New Dish</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

		
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordrePopuler nameCatOrd">Popular</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

		
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordreNew nameCatOrd">New Dish</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

		
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordreOffer nameCatOrd">Offer</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

			



	</div>
</div>



<div id="page2Part3" class="container my-5">
	<div class="row m-auto">
		<div class="col-12 pl-4">
			<h1> FIND NEAR YOU </h1>
		</div>


			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordreNew nameCatOrd">New Dish</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

		
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordrePopuler nameCatOrd">Popular</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

		
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordreNew nameCatOrd">New Dish</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

		
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn ordrePopuler nameCatOrd">Popular</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

			



	</div>
</div>







	<?php include('include/footre.php') ?>
</body>
</html>