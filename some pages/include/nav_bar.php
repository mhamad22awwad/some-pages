<?php  ?>





<nav class="bg-white text-dark navNum2">

	<div class="container py-3 text-dark">
		<div style="" class="ml-4 brand">
			<span class="navbar-brand  h1"><span class="textColor"> eat </span> now</span>
		</div>

		<ul class="nav justify-content-end">
		  	<li class="nav-item pt-2 ">
		  		<a class="" href="#">HOME</a>
		  	</li>
		  	<li class="nav-item pt-2">
		  		<a class="" href="#">RECIPE</a>
		  	</li>
		  	<li class="nav-item pt-2">
		    	<a class="" href="#">TREADING</a>
		  	</li>
		  	<li class="nav-item pt-2 pr-5">
				<button id="hideShow" class="bg-white border-0"><span class="mr-2"><i class="fas fa-shopping-bag"></i></span> CART </button>
		  	</li>

		  	<div id="myDropdown" class=" text-center dropdown-content " style="">

	    		<div id="drobet" class=" px-2 pb-2 m-2 text-left" style="">
  	
  					<div class=" card-header bg-transparent borderCard">
  						<div class="imgCardHold mr-4">
  							<img src="https://via.placeholder.com/100" alt="">
  						</div>
  						<div class="text-left ">
  							<h4> GOOD MEAL </h4>
  							<p> Newrownads </p>
  							<p class=""> VIEW FULL MENU </p>
  						</div>
					</div>

					<div class=" text-dark ">
		    			<h4 class=" order orderTextSize mt-4 mb-4 pt-3 ">Sandwich
							<div class="cartFont">
								<span class="">$35</span>			
							</div>
						</h4>

						<h4 class=" order orderTextSize mt-4 mb-0 pt-3 ">Sub total
							<div class="cartFont">
								<span class="">$35</span>
							</div>
						</h4>

						<p> Extra charges may apply </p>

						<a href="#" class="btn btn-success ordNow w-100 text-white mx-auto mb-2">CHECK OUT NOW</a>
					</div>
				</div>
			</div>

		  	<li class="nav-item pl-3">
				<form class="form-inline justify-content-end ">
					<button type="button" class=" signInNav mt-2 px-3 py-1">SIGN IN</button>
				</form>
			</li>	
		</ul>
	</div>
</nav>







<?php  ?>