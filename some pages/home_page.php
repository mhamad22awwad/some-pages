<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Canvas</title>
	

	<?php include('include/links.php') ?>


</head>


<body id="page1">


<nav class="bg-dark text-white navNum1">

	<div class="container py-3">
		<div style="" class="ml-4 brand">
			<span class="navbar-brand  h1">eat now</span>
		</div>

		<ul class="nav justify-content-end">
		  	<li class="nav-item pt-2 ">
		  		<a class="" href="#">HOME</a>
		  	</li>
		  	<li class="nav-item pt-2">
		  		<a class="" href="#">RECIPE</a>
		  	</li>
		  	<li class="nav-item pt-2">
		    	<a class="" href="#">TREADING</a>
			</li>
			  
			  
			<li class="nav-item pt-2 pr-5">
				<button id="hideShow" class="bg-transparent text-white border-0"><span class="mr-2"><span class="mr-2"><i class="fas fa-shopping-bag"></i></span> CART </button>
		  	</li>

		  	<div id="myDropdown" class=" text-center dropdown-content " style="">

	    		<div id="drobet" class=" px-2 pb-2 m-2 text-left" style="">
  	
  					<div class=" card-header bg-transparent borderCard">
  						<div class="imgCardHold mr-4">
  							<img src="https://via.placeholder.com/100" alt="">
  						</div>
  						<div class="text-left ">
  							<h4> GOOD MEAL </h4>
  							<p> Newrownads </p>
  							<p class=""> VIEW FULL MENU </p>
  						</div>
					</div>

					<div class=" text-dark ">
		    			<h4 class=" order orderTextSize mt-4 mb-4 pt-3 ">Sandwich
							<div class="cartFont">
								<span class="">$35</span>			
							</div>
						</h4>

						<h4 class=" order orderTextSize mt-4 mb-0 pt-3 ">Sub total
							<div class="cartFont">
								<span class="">$35</span>
							</div>
						</h4>

						<p> Extra charges may apply </p>

						<a href="#" class="btn btn-success ordNow w-100 text-white mx-auto mb-2">CHECK OUT NOW</a>
					</div>
				</div>
			</div>
			
			  
			<li class="nav-item">
				<form class="form-inline justify-content-end ">
					<button type="button" class="signInNav mt-2 px-2">SIGN IN</button>
				</form>
			</li>	
		</ul>
	</div>
</nav>




<div id="part1" class="text-white bg-dark h-auto d-inline-block w-100 ">

	<div id="slaider" class="container height">

		<div class="text-center mx-auto " >

			<h1>WORLD LARGEST FOOD CHAIN</h1>

			<p>ORDER FOOD FROM FAVOURITE RESTAURANTS NEAR YOU.</p>
			
					<form class="form-inline" action="#" >
			    		<div class="search-form input-group  mx-auto mt-1" style="width: 75%;">
			    		<input type="text" class="form-control search" onkeyup="showResult(this.value)" placeholder="Enter your delivery addressh">
			   			<div class="input-group-append">
			     		<button type="submit" class="butt searchBut px-4" > SEARCH </button>
			    		</div>
			  			</div>

			  			<div id="livesearch">
			  				
			  			</div>
			  		</form>
		  		
  		</div>
  	</div>




  	<div class="container" >
  		<div class="row align-items-end">
  			
	  		<div class="col-lg-3 col-md-6 col-sm-12 ml-lg-auto">
	  			<div class="imgHold mr-2">
	  				<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  			</div>


		  			<p> FOOD DISH </p>
		  			<div>
		  				
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
		  			</div>
		  			<p><sub> 4.5 (127 Review) </sub></p>
	  		</div>
	  		

	  		<div class=" col-lg-3 col-md-6 col-sm-12">
	  			<div class="imgHold  mr-2">
	  				<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  			</div>

	  			<p> FOOD DISH </p>
	  			<div>
	  				
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
	  			</div>
	  			<p><sub> 4.5 (127 Review) </sub></p>
	  		</div>


	  		<div class=" col-lg-3 col-md-6 col-sm-12 mr-lg-auto">
	  			<div class="imgHold  mr-2">
	  				<img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
	  			</div>

	  			<p> FOOD DISH </p>
	  			
	  			<div>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
	  			</div>
	  			
	  			<p><sub> 4.5 (127 Review) </sub></p>
	  		</div>

  		</div>

	</div>
</div>


<div id="part2" class="h-auto d-inline-block w-100 p-3 divCent colorBack pt-5" >

	<div class="height2" >
		<h2>POPULAR FOOD NOW</h2>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt sed do eiusmod temporut magna aliqua.</p>
	</div>


	<div class="container">
		<div class="">

			
			
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn btn-danger nameCatOrd">New Dish</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>

			
					
			<div class="card pupeularCard ml-4 m-3 text-left" >
  	
  				<div class="card-header bg-transparent border-0 ">
  					<a href="#" class="btn btn-danger nameCatOrd">New Dish</a>
  					<span class="text-right numOfOrder" ><i class="far fa-heart"></i> 21</span>
				</div>

				<div class="card-body text-dark ">
		  			<div class="img2Hold"></div>
		    		<h5 class="mt-4 text-left">CHICKEN PAILLARD</h5>
		    		<div class="my-3">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
		    		<p class="card-text w-a">Flatbread with tomatoes, cheese, oil, garlic, basil and sliced mozzarella.</p>
		  		</div>

		  		<div class="card-footer bg-transparent ">

		  			<div class="imgHold2 rounded">	
		  			</div>

		  			<p class="d-inline">Wild Wings Gril & Ba </p>
					<a href="#" class="butt p-2 btn-success ordNow">ORDER NOW</a>
					<br>
					<span class="small">$ America</span>
				</div>
			
			</div>
	

		</div>
	</div>
</div>


<div id="part3" class="h-auto d-inline-block w-100 p-3 pt-5">
	<div class="container">
		<div class="text-center">
			
			<div class=" m-auto" >
				<h1>EASY THREE STRP FOR ORDER</h1>
				<P class="px-5 pb-5 w-75 m-auto"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt sed do eiusmod temporut magna aliqua.</P>
			</div>
			
			
				<div class=" cardInfo ml-4 m-3">
					<div class="card bgBackRaund">
						
						<img src="img/shop.png" class="Icon mt-3 mx-auto">

						<div class="card-body ">
							<h3 class="card-title order orderTextSize"> CHOOSE A RESTAURANT </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et feugiat mi. Sed lacinia euismod convallis. Nam pharetra aliquamleo, Sed lacinia euismod convallis. </p>
			            </div>
			        	
			        </div>
				</div>

				<div class=" cardInfo m-3">
					<div class="card bgBackMid">

						
						<img src="img/serving-dish.png" class="Icon mt-3 mx-auto">
						
						
						<div class="card-body ">
							<h3 class="card-title order orderTextSize"> CHOOSE A TASTY DISH </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et feugiat mi. Sed lacinia euismod convallis. Nam pharetra aliquamleo, Sed lacinia euismod convallis. </p>
			            </div>
			        	
			        </div>
				</div>

				<div class=" cardInfo mr-4 m-3">
					<div class="card bgBackRaund">

						
						<img src="img/van.png" class="Icon mt-3 mx-auto">
						

						<div class="card-body ">
							<h3 class="card-title order orderTextSize"> PICK UP OR DELIVERY </h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et feugiat mi. Sed lacinia euismod convallis. Nam pharetra aliquamleo, Sed lacinia euismod convallis. </p>
			            </div>
			        	
			        </div>
				</div>

			


			


			

			
		</div>		
	</div>
</div>


<div id="part4" class="h-auto d-inline-block w-100 p-3 divCent colorBack pt-5">
	<div class="container">
		<h1> CHOOSE RESTAURANT IN YOUR AREA </h1>

		<div class="container pt-5 pb-4">
			<div class="row fontMine">
				<span class="col-1"></span>
				<span class="col-2"> ALL </span>
				<span class="col-2"> FAST FOOD </span>
				<span class="col-2"> SHAKES AND SMOOTHIES </span>
				<span class="col-2"> CHINESE </span>
				<span class="col-2"> HEALTHY </span>
				<span class="col-1"></span>
			</div>
		</div>
	</div>	
		

	<div class="container">
		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 btn-light mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 btn-light mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo mr-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 btn-light mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 btn-light mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 btn-light mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo mr-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 btn-light mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

	</div>		

</div>


<div id="part5" class="h-auto d-inline-block w-100 p-3 pt-5">
	<div class="container">
	<div class="row">

		<div class="col-lg-2"></div>
		<div class="col-lg-8 divCent pb-4">
			<h1 class="pb-3">COLLECTIONS FOR YOU </h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt sed do eiusmod temporut magna aliqua.</p>
		</div>
		<div class="col-lg-2"></div>

			
	</div>

		
				<div class="cardInfo ml-4 m-3 border border-secondary ">
					<div class="pl-4">
						<h4> Diet for Two weeks </h4>
						<p> Latest added 4 Days ago </p>
					</div>
					
					<div class="container">
					<div class="row">

						

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" class="">
						</div>

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>


						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>
						
					</div>
					</div>

				</div>

				<div class="cardInfo m-3 border border-secondary ">
					<div class="pl-4">
						<h4> Diet for Two weeks </h4>
						<p> Latest added 4 Days ago </p>
					</div>
					
					<div class="container">
					<div class="row">

						

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" class="">
						</div>

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>


						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>
						
					</div>
					</div>

				</div>

				<div class="cardInfo mr-4 m-3 border border-secondary ">
					<div class="pl-4">
						<h4> Diet for Two weeks </h4>
						<p> Latest added 4 Days ago </p>
					</div>
					
					<div class="container">
					<div class="row">

						

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" class="">
						</div>

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>


						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>

						<div class="col-5 m-2  mx-auto">
							<img src="https://via.placeholder.com/100" alt="">
						</div>
						
					</div>
					</div>

				</div>
	
		


	</div>
</div>


<div id="part6" class="h-auto d-inline-block w-100 p-3 bg-dark">
	<div class="container">
		<div class="row">

			

			<div class="col-lg-5 ml-5 mt-3 colorBack space">
			</div>

			<div class="col-lg-1"></div>

			<div class="col-lg-5 col-md-12 col-sm-12 text-white" >
				<h2>ORDER NOW BY SMART PHONE</h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt sed do eiusmod. tempor incididunt sed do eiusmod </p>
				
				
					<button type="button" class="col-3 download btn buttuns text-white ">DOWNLOAD</button>
					
					

					<button type="button" class="col-3 download btn border border-white text-white ml-4">DOWNLOAD</button>
					
				
			</div>


		</div>
	</div>
</div>




<?php include('include/footre.php') ?>

</body>
</html>