<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Canvas</title>
	

	<?php include('include/links.php') ?>


</head>

<body id="page3">
<?php include('include/nav_bar.php') ?>




	

<div id="page2Part1" class="text-white bg-dark h-auto d-inline-block w-100 mb-5 pb-0">

	<div id="slaider" class="container ">

		<div class="row" >

			<div class="ml-4 my-5 col-5 ">
				<img src="https://via.placeholder.com/300" alt="" class="my-3 width-full">
			</div>

			<div class="col  mt-5">
				<h4 class="mt-3 "> BIKANARE </h4>
				<p class="mt-4"> VISHAL MARKET LOCAL SHOPING CENTER, GTB NAGAR </p>
				<p class="mt-4"> BEST FOOD DISH </p>
				<div id="mineText" class="mb-4">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="ml-1">501 USER</span>
				</div>
				<button type="button" class="p-1 btn-light mr-3 " > 20-30 min </button>
				
			</div>
			
			
					
		  		
  		</div>
  	</div>

  	<div id="page3EndPart1" class="mb-0 pt-2" >
		<div class="text-center">

			<p> <span>Home</span> > <span>New townards</span> > <span>Wild Wings Grill & Ba </span></p>
		
		</div>
	</div>

</div>




	

<div id="page2Part2" class="container my-5">
	<div class="row m-auto">
		<div class="col-12 pl-4">
			<h1 style=""> POPULAR BRANDS </h1>
			<button type="button" class="btn btn-outline-dark mr-3 " style=""> FILTER </button>
		</div>


		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 mb-2 addNow"> ADD NOW </button>
			            </div>
			        </div>
		</div>

		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>


		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>
			

	</div>
</div>



<div id="page2Part3" class="container my-5">
	<div class="row m-auto">
		<div class="col-12 pl-4">
			<h1> FIND NEAR YOU </h1>
		</div>


			

		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		
		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>

		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>


		<div class=" cardInfo ml-4 m-3">
					<div class="card">
						<img src="https://via.placeholder.com/150" class="card-img-top p-4" alt="...">
						<div class="card-body text-left small">
							<h3 class="card-title order orderTextSize">SOY CHORIZO
							<div class="mineText">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
			            	</h3>
			            	<p class="card-text">Lorem ipsum dolor sit amet, consectetur </p>
			            </div>
			        	<div class="row">
			                <p class="col-3 ml-3">$10</p>
			                <div class="col-4"></div>
			                <button type="button" class="butt p-2 addNow mb-2" > ADD NOW </button>
			            </div>
			        </div>
		</div>
			



			



	</div>
</div>







	<?php include('include/footre.php') ?>
</body>
</html>